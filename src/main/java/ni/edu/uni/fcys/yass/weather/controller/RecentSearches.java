/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.fcys.yass.weather.controller;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import ni.edu.uni.fcys.yass.weather.pojo.City;
/**
 *
 * @author LeilaS
 */
public class RecentSearches {
    
    public List<City> getCityList() throws FileNotFoundException {

        File file = new File("src/main/resources/ni/edu/uni/fcys/yass/city.list-recent.json");
        System.out.println((file.exists() && file.length() > 0));
        if (file.exists() && file.length() > 0) {
            Gson gson = new Gson();
            JsonReader reader
                    = new JsonReader(new FileReader(getClass().getResource("/ni/edu/uni/fcys/yass/city.list-recent.json").getPath()));
            City[] data = gson.fromJson(reader, City[].class);
            return new ArrayList(Arrays.asList(data));
        }else{
            return new ArrayList();
        }

    }
    
    public void addCityToJSon(int index) throws IOException{
        
        City c = DlgCityListController.cities.get(index);
        
        File file = new File("src/main/resources/ni/edu/uni/fcys/yass/city.list-recent.json");
        
        if (!file.exists()) {
            file.createNewFile();
        }
        
        
        List<City> lista = getCityList();
        
        for(City currentCity : lista){
            if (currentCity.getId() == c.getId()) {
                System.out.println("Esta ciudad ya esta registrada");
                return;
            }
        }
        
        if (lista.size() >= 5) {
            lista.remove(0);
        }
        
        lista.add(c);
        
        FileWriter writer = new FileWriter(file);
        
        Gson gson = new Gson();
        
        gson.toJson(lista, writer);
        
        writer.flush();
        writer.close();
        
        System.out.println("Se ejecuta la insercion");
    }
}
